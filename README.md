#### Magisk WiFi Bonding

Make 2.4Ghz/5Ghz WiFi running at 40Mhz on your Qualcomm devices!

#### NOTICE

* You should use latest Magisk Manager to install this module. If you meet any problem under installation from Magisk Manager, please try to install it from recovery.
* Recent fixes:
Try to Fix for Nexus 6p users.

#### Credit & Support

* Copyright (C) 2017 simonsmh <simonsmh@gmail.com>
* Any issue or pull request is welcomed.
* Star this module at [GitHub](https://github.com/Magisk-Modules-Repo/magisk-wifi-bonding).
